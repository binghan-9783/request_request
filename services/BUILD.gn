# Copyright (C) 2023 Huawei Device Co., Ltd.
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import("//base/request/request/request_aafwk.gni")
import("//build/ohos.gni")

config("download_service_config") {
  visibility = [ ":*" ]
  include_dirs = [ "include" ]

  ldflags = [ "-Wl,--exclude-libs=ALL" ]
  cflags_cc = [ "-fno-exceptions" ]
  cflags = [
    "-fdata-sections",
    "-ffunction-sections",
    "-fvisibility=hidden",
  ]
}

ohos_rust_shared_library("download_server") {
  sanitize = {
    integer_overflow = true
    ubsan = true
    boundary_sanitize = true
    cfi = true
    cfi_cross_dso = true
    debug = false
  }

  sources = [ "src/lib.rs" ]

  public_configs = [ ":download_service_config" ]

  deps = [ ":request_service_c" ]

  features = [ "oh" ]

  external_deps = [
    "hilog:hilog_rust",
    "hisysevent:hisysevent_rust",
    "hitrace:hitrace_meter_rust",
    "ipc:ipc_rust",
    "netstack:ylong_http_client",
    "safwk:system_ability_fwk_rust",
    "samgr:rust_samgr",
    "ylong_runtime:ylong_runtime",
  ]

  crate_name = "download_server"
  subsystem_name = "request"
  part_name = "request"
}

ohos_static_library("request_service_c") {
  sanitize = {
    integer_overflow = true
    ubsan = true
    boundary_sanitize = true
    cfi = true
    cfi_cross_dso = true
    debug = false
  }
  stack_protector_ret = true

  include_dirs = [
    "../common/include",
    "c_wrapper/include",
  ]
  sources = [
    "c_wrapper/source/application_state_observer.cpp",
    "c_wrapper/source/background_notification.cpp",
    "c_wrapper/source/c_check_permission.cpp",
    "c_wrapper/source/c_event_handler.cpp",
    "c_wrapper/source/c_request_database.cpp",
    "c_wrapper/source/c_string_wrapper.cpp",
    "c_wrapper/source/c_task_config.cpp",
    "c_wrapper/source/c_task_info.cpp",
    "c_wrapper/source/common_event_notify.cpp",
    "c_wrapper/source/get_calling_bundle.cpp",
    "c_wrapper/source/get_proxy.cpp",
    "c_wrapper/source/get_top_bundle.cpp",
    "c_wrapper/source/network_adapter.cpp",
  ]
  cflags_cc = [ "-O2" ]

  external_deps = [
    "ability_base:base",
    "ability_base:want",
    "ability_base:zuri",
    "ability_runtime:ability_manager",
    "ability_runtime:app_manager",
    "access_token:libaccesstoken_sdk",
    "access_token:libtokenid_sdk",
    "c_utils:utils",
    "common_event_service:cesfwk_innerkits",
    "distributed_notification_service:ans_innerkits",
    "eventhandler:libeventhandler",
    "hilog:libhilog",
    "ipc:ipc_single",
    "netmanager_base:net_conn_manager_if",
    "relational_store:native_appdatafwk",
    "relational_store:native_dataability",
    "relational_store:native_rdb",
    "safwk:system_ability_fwk",
    "samgr:samgr_proxy",
  ]
  defines = []
  if (request_telephony_core_service && request_telephony_cellular_data) {
    external_deps += [
      "cellular_data:tel_cellular_data_api",
      "core_service:tel_core_service_api",
    ]
    defines += [ "REQUEST_TELEPHONY_CORE_SERVICE" ]
  }
  output_extension = "so"
  subsystem_name = "request"
  part_name = "request"
}
